
$(document).ready(function(){
/*
var $btnTop = $('.btn-go');      //start button go to top scroll
$(window).on('scroll', function(){
    if($(window).scrollTop() >= 300){
        $btnTop.fadeIn();
    }else{
        $btnTop.fadeOut();
    }
});

$btnTop.on('click', function(){
    $('html,body').animate({scrollTop:0},1000);
});  
*/

$(window).scroll(function(){
    //console.log($(this).scrollTop());//info console scroll px

    //parallax_1();//parallax css transform : translate
    //parallax_2();//parallax css top
    //parallax_3();//test variant 
    parallax_4();

    
});

function parallax_1(){
    var st = $(this).scrollTop();
    
    $(".transform-h1").css({
        "transform" : "translate(0%, " + st/19 +"%"
    });

    $(".transform-h3").css({
        "transform" : "translate(0%, " + st/46 +"%"
    });

    if($(window).scrollTop() <= 900){
        $(".section1").css({
            "transform" : "translate(0%, " + st/19 +"%"
        });
    }else{
        $(".section1").css({
            "transform" : -"translate(0%, " + st/19 +"%"
        });
    }
    if($(window).scrollTop() <= 2402){
        $(".section3").css({
            'top':'-61em',
            "transform" : "translate(0%, " + st/19 +"%"
        });
    }else{
        $(".section3").css({
            'top':'-61em',
            "transform" : -"translate(0%, " + st/19 +"%"
        });
    }
}

function parallax_2(){
    $(".section1").css({'top': $(window).scrollTop()/4});
}

function parallax_3(){
    if($(window).scrollTop() >= 800){
        $(".section3").css({'top': $(window).scrollTop()/3});
    }else{
        $(".section3").css({'top': $(window).scrollTop()/3});
    }
}

function parallax_4(){

    var wScroll = $(window).scrollTop();
    var w_height = $(window).height();

    if($(window).width() <= 700){

        $('.section1').css('top' , +(w_height)/1700+(wScroll)*0.030+'em');
        $('.section3').css('top' , -(w_height)/16.8+(wScroll)*0.030+'em');
        
    }else {
        if($(window).scrollTop() <= 3524){
            $('.section1').css('top' , +(w_height)/1700+(wScroll)*0.030+'em');

        }else{
            $('.section1').css('top' , +0-(wScroll)*0.000+'em');

        }
            $('.section3').css('top' , -(w_height)/16.8+(wScroll)*0.030+'em');

    }
        

};





});


/*
$(window).on('mousemove',function(e){
    var h = $(window).height();
    var w = $(window).width();

    var offsetX = 0.5 - e.pageX / w;
    var offsetY = 0.5 - e.pageY / h;
    $(".transform-h1").each(function(i,el){

        var offset = parseInt($(el).data('offset'));

        var translate = "translate3d(" + Math.round(offsetX * offset)
        + "px," + Math.round(offsetY * offset)+ "px , 0px";
        
        $(el).css({
            'transform':translate
        });
    });
});
*/
