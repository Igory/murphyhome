

let textarea = document.getElementById('textarea'),
DropRu = document.querySelector(".DropRu"),
DropUa = document.querySelector(".DropUa"),
DropEu = document.querySelector(".DropEu"),
DropAdd = document.querySelector(".DropAdd"),
Add = document.querySelector(".Add"),
RenderText = document.getElementById("RenderText"),
LenghtText = document.getElementById("LenghtText"),
showDiw = document.querySelector(".showDiw"),
Addtextarea = document.getElementById("Addtextarea"),
Seconds = document.getElementById("Seconds"),
Minutes = document.getElementById("Minutes"),
textInfo = document.getElementById("textInfo"),
numMiss = document.getElementById("numMiss"),
resultMin = document.getElementById("resultMin"),
resultSec = document.getElementById("resultSec"),
resul = document.querySelector(".resul"),
inlineRadio1 = document.getElementById("inlineRadio1"),
inlineRadio2 = document.getElementById("inlineRadio2");

let arrKey =[];
let arrLenghtText=[];
let arrJoinText = [];
let arrCreateName = [];
let arrWarning = [];
let SecTimeAdd = 0;
let MinTimeAdd = 0;
let regexpUa = /Ua/i;
let regexpRu = /Ru/i;
let regexpEu = /Eu/i;
let regexpAdd = /Add/i;

let Obj = {
    RuText1:{
        Heading:'Я помню чудное мгновенье',
        Text:
        `
        Я помню чудное мгновенье:
        Передо мной явилась ты,
        Как мимолетное виденье,
        Как гений чистой красоты.
        
        В томленьях грусти безнадежной
        В тревогах шумной суеты,
        Звучал мне долго голос нежный
        И снились милые черты.
        
        Шли годы. Бурь порыв мятежный
        Рассеял прежние мечты,
        И я забыл твой голос нежный,
        Твои небесные черты.
        
        В глуши, во мраке заточенья
        Тянулись тихо дни мои
        Без божества, без вдохновенья,
        Без слез, без жизни, без любви.
        
        Душе настало пробужденье:
        И вот опять явилась ты,
        Как мимолетное виденье,
        Как гений чистой красоты.
        
        И сердце бьется в упоенье,
        И для него воскресли вновь
        И божество, и вдохновенье,
        И жизнь, и слезы, и любовь.`
    },
    RuText2:{
        Heading:'У лукоморья дуб зеленый',
        Text:
        `
        У лукоморья дуб зелёный;
        Златая цепь на дубе том:
        И днём и ночью кот учёный
        Всё ходит по цепи кругом;
        Идёт направо - песнь заводит,
        Налево - сказку говорит.
        Там чудеса: там леший бродит,
        Русалка на ветвях сидит;
        Там на неведомых дорожках
        Следы невиданных зверей;
        Избушка там на курьих ножках
        Стоит без окон, без дверей;
        Там лес и дол видений полны;
        Там о заре прихлынут волны
        На брег песчаный и пустой,
        И тридцать витязей прекрасных
        Чредой из вод выходят ясных,
        И с ними дядька их морской;
        Там королевич мимоходом
        Пленяет грозного царя;
        Там в облаках перед народом
        Через леса, через моря
        Колдун несёт богатыря;
        В темнице там царевна тужит,
        А бурый волк ей верно служит;
        Там ступа с Бабою Ягой
        Идёт, бредёт сама собой,
        Там царь Кащей над златом чахнет;
        Там русский дух... там Русью пахнет!
        И там я был, и мёд я пил;
        У моря видел дуб зелёный;
        Под ним сидел, и кот учёный
        Свои мне сказки говорил.`
    },
    UaText3:{
        Heading:'Як умру, то поховайте',
        Text:
        `
        Як умру, то поховайте
        Мене на могилі
        Серед степу широкого
        На Вкраїні милій,
        Щоб лани широкополі,
        І Дніпро, і кручі
        Було видно, було чути,
        Як реве ревучий.
        Як понесе з України
        У синєє море
        Кров ворожу... отойді я
        І лани і гори —
        Все покину, і полину
        До самого Бога
        Молитися... а до того
        Я не знаю Бога.
        Поховайте та вставайте,
        Кайдани порвіте
        І вражою злою кров’ю
        Волю окропіте.
        І мене в сем’ї великій,
        В сем’ї вольній, новій,
        Не забудьте пом’янути
        Незлим тихим словом.`
    },
    EuText4:{
        Heading:'Two roads diverged in a yellow wood',
        Text:
        `
        Two roads diverged in a yellow wood,
        And sorry I could not travel both
        And be one traveler, long I stood
        And looked down one as far as I could
        To where it bent in the undergrowth.
        Then took the other, as just as fair,
        And having perhaps the better claim,
        Because it was grassy and wanted wear;
        Though as for that the passing there
        Had worn them really about the same.
        And both that morning equally lay
        In leaves no step had trodden black.
        Oh, I kept the first for another day!
        Yet knowing how way leads on to way,
        I doubted if I should ever come back.
        I shall be telling this with a sigh
        Somewhere ages and ages hence:
        Two roads diverged in a wood, and I--
        I took the one less traveled by,
        And that has made all the difference.`
    },
    RuText5:{
        Heading:'Белая берёза',
        Text:
        `
        Белая берёза
        Под моим окном
        Принакрылась снегом,
        Точно серебром.
        
        На пушистых ветках
        Снежною каймой
        Распустились кисти
        Белой бахромой.
        
        И стоит береза
        В сонной тишине,
        И горят снежинки
        В золотом огне.
        
        А заря, лениво
        Обходя кругом,
        обсыпает ветки
        Новым серебром.
        `
    },
    EuText6:{
        Heading:`William Blake - The Smile`,
        Text:`
        There is a smile of love,
        And there is a smile of deceit,
        And there is a smile of smiles
        In which these two smiles meet.
        
        And there is a frown of hate,
        And there is a frown of disdain,
        And there is a frown of frowns
        Which you strive to forget in vain,
        
        For it sticks in the heart's deep core
        And it sticks in the deep backbone
        And no smile that ever was smil'd,
        But only one smile alone,
        
        That betwixt (уст. между) the cradle and grave
        It only once smil'd can be;
        And, when it once is smil'd,
        There's an end to all misery.`
    },
    EuText7:{
        Heading:`An Old Story`,
        Text:`
        Strange that I did not know him then,
        That friend of mine!
        I did not even show him then
        One friendly sign;
        
        But cursed him for the ways he had
        To make me see
        My envy of the praise he had
        For praising me.
        
        I would have rid the earth of him
        Once, in my pride!...
        I never knew the worth of him
        Until he died.`
    },
    EuText8:{
        Heading:`A Duet`,
        Text:`
        A little yellow Bird above,
        A little yellow Flower below;
        The little Bird can sing the love
        That Bird and Blossom know;
        The Blossom has no song nor wing,
        But breathes the love he cannot sing.`
    },
    EuText9:{
        Heading:`Dust Of Snow – Robert Frost`,
        Text:`
        The way a crow
        Shook down on me
        The dust of snow
        From a hemlock tree
        Has given my heart
        A change of mood
        And saved some part
        Of a day I had rued.`
    },
    RuText10:{
        Heading:`Вот она, вот она...`,
        Text:`
        Вот она, вот она -
        Наших душ глубина,
        В ней два сердца плывут, как одно,-
        Пора занавесить окно.
        
        Пусть в нашем прошлом будут рыться люди странные,
        И пусть сочтут они, что стоит все его приданное,-
        Давно назначена цена
        И за обоих внесена -
        Одна любовь, любовь одна.
        
        Холодна, холодна
        Голых стен белизна,-
        Но два сердца стучат, как одно,
        И греют, и - настежь окно!
        
        Но перестал дарить цветы он просто так, не к случаю,
        Любую женщину в кафе теперь считает лучшею.
        И улыбается она
        Случайным людям у окна,
        И привыкает засыпать одна.`,
    },
    RuText11:{
        Heading:`У себя`,
        Text:`
        Дождались меня белые ночи
        Над простором густых островов…
        Снова смотрят знакомые очи,
        И мелькает былое без слов.
        
        В царство времени всё я не верю,
        Силу сердца еще берегу,
        Роковую не скрою потерю,
        Но сказать "навсегда" — не могу.
        
        При мерцании долгом заката,
        Пред минутной дремотою дня,
        Что погиб его свет без возврата,
        В эту ночь не уверишь меня.`,
    },
    UaText12:{
        Heading:`Сонячний дім`,
        Text:`
        Привітний і світлий наш сонячний дім,
        Як радісно й весело жити у нім.
        Тут мамина пісня і усмішка тата.
        В любові й добрі тут зростають малята.
        Дзвінка наша пісня до сонечка лине:
        "Мій сонячний дім – це моя Україна!"`,
    },
    UaText13:{
        Heading:`Тобою`,
        Text:`
        Послухай, як струмок дзвенить,
        Як гомонить ліщина.
        З тобою всюди, кожну мить
        Говорить Україна.
        
        Послухай, як трава росте,
        Напоєна дощами,
        І як веде розмову степ
        З тобою колосками.
        
        Послухай, як вода шумить –
        Дніпро до моря лине, –
        З тобою всюди, кожну мить
        Говорить Україна.`,
    },
    UaText14:{
        Heading:`Садок вишневий коло хати`,
        Text:`
        Садок вишневий коло хати,
        Хрущі над вишнями гудуть,
        Плугатарі з плугами йдуть,
        Співають ідучи дівчата,
        А матері вечерять ждуть.
        Сім'я вечеря коло хати,
        Вечірня зіронька встає.
        Дочка вечерять подає,
        А мати хоче научати,
        Так соловейко не дає.
        Поклала мати коло хати
        Маленьких діточок своїх;
        Сама заснула коло їх.
        Затихло все, тілько дівчата
        Та соловейко не затих.`,
    },
    UaText15:{
        Heading:`Щоб довелося мандрувати`,
        Text:`
        Щоб довелося мандрувати –
        Піти по рідній всій землі:
        У кожне місто завітати,
        У кожнім побувать селі, –
        
        То навіть би за сотню років
        Цього б не встигли ми зробить:
        Простори в нас такі широкі,
        А міст і сіл – що не злічить!`,
    }
    
}
counterAdd= 0;
function Add_your(){
    Add.addEventListener("click" , () =>{
        if (Addtextarea.textLength === 0){
            alert("To add your own text, first paste it into the input field");
        }else{
            counterAdd++;
            let AddKeyObj = "AddText" + counterAdd;
            let SplitTextarea = Addtextarea.value.split(" ");
            for(let s = 0; s < 4 ; s++){
                let pushTextT = SplitTextarea[s];
                arrCreateName.push(pushTextT);
                let CreateName = arrCreateName.join(" ");
                Obj[AddKeyObj] = {Heading: CreateName ,Text: Addtextarea.value};   
            }
            Addtextarea.value = "";
        }
        arrCreateName.length = 0;
        addDropDownItem();
    });
}Add_your();

function addDropDownItem(){
    let arrKey = Object.keys(Obj);
    DropRu.innerHTML="";
    DropUa.innerHTML ="";
    DropEu.innerHTML ="";
    DropAdd.innerHTML = "";
    for(let arr of arrKey){
        if(regexpRu.test(arr) == true){
            let arrLenghtText = Object.values(Obj[arr].Text).join("").split('        ').join("");
            DropRu.innerHTML += `<a class="dropdown-item" data-key=`+arr+` href="#">`+ Obj[arr].Heading +` <span>`+arrLenghtText.length+`</span> </a>`
        }else if(regexpUa.test(arr) == true){
            let arrLenghtText = Object.values(Obj[arr].Text).join("").split('        ').join("");
            DropUa.innerHTML += `<a class="dropdown-item" data-key=`+arr+` href="#">`+ Obj[arr].Heading +` <span>`+arrLenghtText.length+`</span> </a>`
        }else if(regexpEu.test(arr) == true){
            let arrLenghtText = Object.values(Obj[arr].Text).join("").split('        ').join("");
            DropEu.innerHTML += `<a class="dropdown-item" data-key=`+arr+` href="#">`+ Obj[arr].Heading +` <span>`+arrLenghtText.length+`</span> </a>`
        }else if(regexpAdd.test(arr) == true){
            let arrLenghtText = Object.values(Obj[arr].Text).join("").split('        ').join("");
            DropAdd.innerHTML += `<a class="dropdown-item" data-key=`+arr+` href="#">`+ Obj[arr].Heading +` <span>`+arrLenghtText.length+`</span> </a>`
        }
    }

    function clickDropItem(){
        let DropItem = document.querySelectorAll(".dropdown-item");
        DropItem.forEach((item)=>{
            item.addEventListener("click" , ()=>{
                time();
                RenderText.innerHTML = "";
                textarea.dataset.stop = "false";
                textarea.value = "";
                setTimeout(() => { clearInterval(timerid);}, 1);
                numMiss.innerHTML ="Mistakes: ";
                SecTimeAdd = 0;
                MinTimeAdd = 0;
                Minutes.textContent = "00";
                Seconds.textContent = "00";

                let itemDataset = item.dataset.key;
                let openLenghtText = Object.values(Obj[itemDataset].Text).join("").split('        ').join("").split("\n");
                let arrLenghtText = Object.values(Obj[itemDataset].Text).join("").split('        ').join("");

                dataCounter = false;
                openLenghtText.forEach((text)=>{
                    RenderText.innerHTML += `<p data-counter=`+ dataCounter +`>`+text+`</p>`;
                });
                showDiw.style.display = "block";
                LenghtText.textContent = arrLenghtText.length;

                let search_P = document.querySelectorAll("#RenderText P");
                for(let i = 0; i < search_P.length; i++){
                    if(search_P[i].outerText == ""){
                        search_P[i].remove();
                    }
                }
                Span();
                textarea.disabled = false;
            });
        });
    }clickDropItem();
    arrKey.length = 0;
    arrLenghtText.length = 0;
}addDropDownItem();

function textareaStart(){
    textarea.addEventListener("keyup" ,() =>{
        
        time();
        testText();
    });
}textareaStart();


function testText(){
    let search_P = document.querySelectorAll("#RenderText P");
    let lengthTextarea = textarea.textLength -1;
    textarea.dataset.stop = "wait";
    if(lengthTextarea >= 0){
        let search_span = document.querySelectorAll("#RenderText P SPAN");
        let splitTextarea = textarea.value.split("");
        search_span.forEach((classSpan) =>{
            if(classSpan.classList.length >= 1){
                classSpan.classList.remove("Plabel");
                classSpan.classList.remove("textTrue");
                classSpan.classList.remove("textFalse");
            }
        });

        watchSymbolAndChecked();
        
        for(let i = 0; i < search_span.length; i++){
            if(search_span.length === splitTextarea.length){

                arrJoinText.push(search_span[i].textContent);
                let JoinText = arrJoinText.join("");

                if(JoinText === textarea.value){
                    if(inlineRadio1.checked === true){ //No clues checked 1
                        search_span[0].parentElement.classList.add("Plabel");
                    }else if(inlineRadio2.checked === true){ //With tips checked 2
                        search_span[0].parentElement.classList.remove("pFalse");
                        search_span[0].parentElement.classList.add("pTrue");
                    }
                    for(let p_span of search_P){
                        if(p_span.dataset.counter == "false"){
                            p_span.dataset.counter = "true";
                            break;
                        }
                    }
                    textarea.value = "";
                }else{
                    if(inlineRadio1.checked === true){ //No clues checked 1
                        search_span[0].parentElement.classList.add("Plabel");
                    }else if(inlineRadio2.checked === true){ //With tips checked 2
                        search_span[0].parentElement.classList.remove("pTrue");
                        search_span[0].parentElement.classList.add("pFalse");
                    }
                }
                
            }
        }
        Span();
        arrJoinText.length =0;
    }
}

function watchSymbolAndChecked(){
    let lengthTextarea = textarea.textLength -1;
    let search_span = document.querySelectorAll("#RenderText P SPAN");
    let splitTextarea = textarea.value.split("");

    if(inlineRadio1.checked === true){ //No clues checked 1
        if(lengthTextarea < search_span.length){
            if(search_span[lengthTextarea].textContent === splitTextarea[lengthTextarea]){
                search_span[lengthTextarea].classList.add("Plabel");
            }else if (search_span[lengthTextarea].textContent !== splitTextarea[lengthTextarea]){
                search_span[lengthTextarea].classList.add("Plabel");
                arrWarning.push(splitTextarea[lengthTextarea]);            }
        }else{
            console.warn("GOOD");
        }
    }else if(inlineRadio2.checked === true){ //With tips checked 2
        if(lengthTextarea < search_span.length){
            if(search_span[lengthTextarea].textContent === splitTextarea[lengthTextarea]){
                search_span[lengthTextarea].classList.add("textTrue");
                search_span[lengthTextarea].classList.remove("WarningBorder");
            }else if (search_span[lengthTextarea].textContent !== splitTextarea[lengthTextarea]){
                search_span[lengthTextarea].classList.add("textFalse");
                search_span[lengthTextarea].classList.add("WarningBorder");
                arrWarning.push(splitTextarea[lengthTextarea]);
            }
        }else{
            console.warn("GOOD");
        }
    }
}

function Span(){
    let search_P = document.querySelectorAll("#RenderText P");
    if(textarea.textLength === 0){
        for(let p_span of search_P){
            if(p_span.dataset.counter == "false"){

                let split_p_span = p_span.textContent.split("");
                p_span.textContent ="";
                split_p_span.forEach((sp) =>{
                    p_span.innerHTML += `<span>`+sp+`</span>`;
                });
                break;
            }else if(p_span.dataset.counter == "true"){
                if(p_span.firstChild.tagName == "SPAN"){
                    p_span.textContent = arrJoinText.join("");
                }
            }
        }

        let last_p = search_P.length -1;
        if(search_P[last_p].dataset.counter == "true"){
            textarea.disabled = true;
            textarea.dataset.stop = "true";
            EnterInfo();
            time();
        }
    }
}

function time(){
    if(textarea.dataset.stop === "false"){
        timerid = setInterval(() =>{
            SecTimeAdd++;
            if(SecTimeAdd < 10){
                Seconds.textContent ="0"+SecTimeAdd;
            }else{
                Seconds.textContent =SecTimeAdd;
            }
            if(SecTimeAdd === 60){
                MinTimeAdd++;
                SecTimeAdd=0;
                if(MinTimeAdd < 10){
                    Minutes.textContent = "0"+MinTimeAdd;
                }else{
                    Minutes.textContent = MinTimeAdd;
                }
                
            }
        }, 1000);
    }else if(textarea.dataset.stop === "true"){
        setTimeout(() => { clearInterval(timerid);}, 1);
    }
}

function EnterInfo(){
    resul.style.display = "block";
    textInfo.innerHTML = `<p> Number of mistakes: ` +arrWarning.length+`</p>`
    for(let w = 0 ; w < arrWarning.length; w++){
        numMiss.innerHTML += `<span>`+arrWarning[w]+` | </span>`
    }

    if(SecTimeAdd < 10){
        resultSec.innerHTML = "0"+SecTimeAdd;
    }else{
        resultSec.innerHTML = SecTimeAdd;
    }

    if(MinTimeAdd < 10){
        resultMin.innerHTML = "0"+MinTimeAdd;
    }else{
       resultMin.innerHTML = MinTimeAdd; 
    }
    
    SecTimeAdd = 0;
    MinTimeAdd = 0;
    Minutes.textContent = "00";
    Seconds.textContent = "00";
    arrWarning.length = 0;
}